#Prerequisites: 
* Java 11

#How to build project jar: 
Run command below through command line from the /intercom folder. Generated jar will be located @ intercomparty/build/libs/intercom-party-1.0.jar.
```bash
gradlew jar
```

#How to run unit tests:
Run command below through command line from the /intercom folder.
```bash
gradlew test
```

#How to run jar from console / run from bash/.bat file

Run command below

**'path to customer input file'** refers to the list of customers which includes name, userid, longitude and latitude. 

EG. 

* windows: c:\Downloads\customer.txt

* mac/linux: /users/downloads/customers.txt



**'path to output file'** refers to the file you want to save the party invite list to.

EG.

* windows: c:\Downloads\partylist.txt

* mac/linux: /users/downloads/partylist.txt


```bash
java -jar .\intercom-party-1.0.jar <path to customer input file> <path to output file>
```