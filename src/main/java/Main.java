import service.IntercomPartyService;

import java.io.IOException;
import java.util.logging.Logger;

import static java.lang.System.exit;

public class Main {

    private static Logger logger = Logger.getLogger("Main");

    public static void main(String[] args) {

        if(args.length != 2){
            logger.severe("Please run Intercom Party Invite App again with valid input and output file paths");
            exit(0);
        }

        IntercomPartyService intercomPartyService = new IntercomPartyService();

        try{
            logger.info("Process Intercom Party Invite List ....");

            intercomPartyService.createInviteList(args[0], args[1]);

            logger.info("Intercom Party Invite List Complete !!!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
