package model;

import com.google.gson.annotations.SerializedName;

public class Customer implements Comparable<Customer> {

    private String name;
    @SerializedName("user_id")
    private Long userId;
    private double latitude;
    private double longitude;


    public Customer() {
    }

    public Customer(String name, Long userId, double latitude, double longitude) {
        this.name = name;
        this.userId = userId;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString(){
        return "UserId: " + userId + ", Name: " + name;
    }


    @Override
    public int compareTo(Customer o) {
        if(userId == null || o.getUserId() == null){
            return 0;
        }

        return userId.compareTo(o.getUserId());
    }
}
