package service;

import com.google.gson.Gson;
import model.Customer;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;


public class CustomerFileService {

    private static Logger logger = Logger.getLogger("CustomerFileService");

    List<String> extractCustomerList(File customerFile) throws IOException {
        try(BufferedReader reader = new BufferedReader(new FileReader(customerFile))) {
            return reader.lines().collect(Collectors.toList());
        }
    }

    public List<Customer> parseCustomerList(List<String> customerStringList){
        Gson gson = new Gson();
        List<Customer> customerInfo = new ArrayList<>();
        customerStringList.forEach(customerString -> customerInfo.add(gson.fromJson(customerString, Customer.class)));
        return customerInfo;
    }

    //outputs all users to an output file and the console
    public void createOutput(List<Customer> customers, File outputFile) throws IOException {
        // sort ascending as per requirements
        Collections.sort(customers);

        logger.info("Creating Super Intercom Party Invitation List");

        try(FileWriter fileWriter = new FileWriter(outputFile)) {
            for (Customer customer : customers) {
                fileWriter.write(customer.toString() + "\n");
            }
        }

        logger.info("Super Intercom Party Invitation List is now created");
    }
}
