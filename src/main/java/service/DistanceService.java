package service;

public class DistanceService {

    // convert to km:  1° = 111 km = 60 nautical miles
    private static double DEGREES_TO_KM = 111;


    //Compute using law of cosines
    public double calcKMDistanceBetweenPoints(double lat1, double long1, double lat2, double long2 ) {

        validateLatitude(lat1);
        validateLatitude(lat2);
        validateLongitude(long1);
        validateLongitude(long2);

        double lat1_rad = Math.toRadians(lat1);
        double long1_rad = Math.toRadians(long1);
        double lat2_rad = Math.toRadians(lat2);
        double long2_rad = Math.toRadians(long2);

        double radius = Math.acos(
                Math.sin(lat1_rad) * Math.sin(lat2_rad) +
                Math.cos(lat1_rad) * Math.cos(lat2_rad) *
                        Math.cos(long1_rad - long2_rad));


        double degrees = Math.toDegrees(radius);

        return degrees * DEGREES_TO_KM;
    }


    private void validateLatitude(double latitude) {
        if(Math.abs(latitude) > 90) {
            throw new IllegalArgumentException("Latitude: " + latitude + " is invalid. Please specify a range between 0 to +/-90");
        }
    }


    private void validateLongitude(double longitude) {
        if( Math.abs(longitude) > 180) {
            throw new IllegalArgumentException("Longitude: " + longitude + " is invalid. Please specify a range between 0 to +/-180");
        }
    }

}
