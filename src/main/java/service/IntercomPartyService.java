package service;

import model.Customer;

import java.io.*;
import java.util.List;

public class IntercomPartyService {

    //other options for setting these  values are a properties file or user input.
    private static double DUBLIN_LAT = 53.339428;
    private static double DUBLIN_LONG = -6.257664;
    private static double MAX_INVITE_DISTANCE = 100;
    
    
    public void createInviteList (String customerInputFilePath, String customerOutputFilePath) throws IOException {

        //In a spring app we could autowire these services
        CustomerFileService customerFileService = new CustomerFileService();
        DistanceService distanceService = new DistanceService();

        //Process the customer file to extract all relevant info into a Customer list
        File file = new File(customerInputFilePath);

        if(!file.exists()){
            throw new FileNotFoundException("File at: " + file.getPath() + " does not exist. Please try again with a valid file.");
        }

        List<String> customerListStrings  = customerFileService.extractCustomerList(file);
        List<Customer> customerList = customerFileService.parseCustomerList(customerListStrings);

        //Remove customers over 100km away
        customerList.removeIf(customer -> distanceService.calcKMDistanceBetweenPoints(customer.getLatitude(), customer.getLongitude(), DUBLIN_LAT, DUBLIN_LONG) > MAX_INVITE_DISTANCE);

        //Create output file
        File outputFile = new File(customerOutputFilePath);
        customerFileService.createOutput(customerList, outputFile);
        
    }
}
