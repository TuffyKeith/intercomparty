import model.Customer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import service.CustomerFileService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;


class CustomerFileServiceTest {

    private CustomerFileService customerFileService;

    List<String> testCustomerStringList () {
        return List.of("{\"latitude\": \"52.986375\", \"user_id\": 12, \"name\": \"Christina McArdle\", \"longitude\": \"-6.043701\"}",
                       "{\"latitude\": \"51.92893\", \"user_id\": 1, \"name\": \"Alice Cahill\", \"longitude\": \"-10.27699\"}");
    }

    List<Customer> testCustomerList() {
        Customer c1 = new Customer("Christina McArdle", 12L, 52.986375, -6.043701);
        Customer c2 = new Customer("Alice Cahill", 1L, 51.92893, -10.27699);
        return Arrays.asList(c1, c2);
    }



    @BeforeEach
    void setup () {
        customerFileService = new CustomerFileService();
    }


    @Test
    void expectParsedCustomerList() {
        List<Customer> parsedCustomerList =  customerFileService.parseCustomerList(testCustomerStringList());

        assertEquals(2, parsedCustomerList.size());
        assert(parsedCustomerList.stream().map(Customer::getUserId).collect(Collectors.toList())).containsAll(List.of(1L, 12L));

    }


    @Test
    void expectOutputCreated(@TempDir Path tempDir) throws IOException {
        List<Customer> customerList = testCustomerList();
        Path path = tempDir.resolve("output.txt");
        Files.createFile(path);
        List<String> expectedLinesInFile = List.of( "UserId: 1, Name: Alice Cahill", "UserId: 12, Name: Christina McArdle");

        customerFileService.createOutput(customerList, path.toFile() );

        assertTrue( Files.exists(tempDir));
        assertLinesMatch(expectedLinesInFile, Files.readAllLines(path));
    }

}
