import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import service.DistanceService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class DistanceServiceTest {

    private DistanceService distanceService;

    @BeforeEach
    void setup() {
        distanceService = new DistanceService();
    }

    @Test
    void expectedDistance() {
        assertEquals(2950.0942836035 ,distanceService.calcKMDistanceBetweenPoints(1, 1, 20, 20 ));
        assertEquals(19980 ,distanceService.calcKMDistanceBetweenPoints(-90, -180, 90, 180 ));
    }

    @Test()
    void expectedExceptionInvalidLat() {
        assertThrows(IllegalArgumentException.class , () -> distanceService.calcKMDistanceBetweenPoints(1, 1, 90.1, 20 ));
        assertThrows(IllegalArgumentException.class , () -> distanceService.calcKMDistanceBetweenPoints(1, 1, -90.1, 20 ));

        assertThrows(IllegalArgumentException.class , () -> distanceService.calcKMDistanceBetweenPoints(90.1, 1, 20, 20 ));
        assertThrows(IllegalArgumentException.class , () -> distanceService.calcKMDistanceBetweenPoints(-90.1, 1, 20, 20 ));

    }

    @Test()
    void expectedExceptionInvalidLong() {
        assertThrows(IllegalArgumentException.class , () -> distanceService.calcKMDistanceBetweenPoints(1, 180.1, 20, 20 ));
        assertThrows(IllegalArgumentException.class , () -> distanceService.calcKMDistanceBetweenPoints(1, -180.1, 20, 20 ));

        assertThrows(IllegalArgumentException.class , () -> distanceService.calcKMDistanceBetweenPoints(1, 1.0, 20.0,  180.1 ));
        assertThrows(IllegalArgumentException.class , () -> distanceService.calcKMDistanceBetweenPoints(1, 1.0, 20.0, -180.1 ));
    }

}
